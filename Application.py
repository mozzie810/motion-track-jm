# jwhmorris - 2019
# Motion Detection/Tracking/Prediction with OpenCV 
# Background Subtraction/Mosse/Kalman methods

from tkinter import *
from tkinter import filedialog
from Kalman import Kalman
import cv2
import opencv_wrapper as cvw
from CentroidTracker import CentroidTracker
from Evaluator import Evaluator
import numpy as np
from PIL import Image, ImageTk

# ------------------------------------------------------- #

class Application():

    # Video command to call for tracking.
    def MainFunctions(self):
        global checkBgs, checkMosse, checkKalman, checkMerged, threshSpin, checkAcc

        if checkAcc.get() == 1:
            print("Warning: This option will automatically set all other checkboxes ticked.")
            checkBgs.set(1)
            checkMosse.set(1)
            checkKalman.set(1)
            checkMerged.set(1)

        filename =  filedialog.askopenfilename(
            initialdir = "E:/",title = "choose your file", 
            filetypes = (("video files", "*.mp4;*.mpg;*.wmv;*.avi;*.webm"),("all files","*.*")))
        cap = cv2.VideoCapture(filename)
        ret, frame = cap.read()

        # Create a rectangle representing the boundary 1 pixel in.
        boundary = cvw.Rect(1, 1, frame.shape[1] - 2, frame.shape[0] - 2)

        # BGS With params to alter history/threshold/shadows - mess around to test contour results.
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
        fgbg = cv2.createBackgroundSubtractorMOG2(history=100,varThreshold=40,detectShadows=True)

        # init centroid tracking system (200 frames to deregister), and kalman tracker.
        ct = CentroidTracker(200)
        kt = Kalman()
        
        # Threshold settings
        threshArea = 2500
        if threshSpin.get().isdigit():
            threshArea = int(threshSpin.get())

        # create array for arrays of bounding boxes per frame.
        mergedBoxesArrays = []
        while(1):
            ret, frame = cap.read()
            key = cv2.waitKey(30) & 0xff

            if not ret:
                break
            
            # Apply background sub
            fgmask = fgbg.apply(frame.copy(),None,-1)
            
            # change grey (shadows) to black, as MOG2 detectshadows false is bad.
            fgmask[fgmask==127]=0

            # Do morphological opening by eroding and dilating.
            fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_CLOSE, kernel)
            fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
            fgmask = cv2.dilate(fgmask, kernel, iterations=1)

            # Do Gaussian Otsu algorithm to further prevent noise.
            img_blur = cv2.GaussianBlur(fgmask,(5,5), 0)
            # threshold(image, threshold, maxval, thresholdtypes)
            ret, img_blur_otsu_thresh = cv2.threshold(img_blur, 127, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

            contour, heir = cv2.findContours(img_blur_otsu_thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
            bboxes = []

            # Get each contour (stored as a vector of points) from contours from bin image.
            for cnt in contour:

                # Use convex hull to complete contour shape
                cnt = cv2.convexHull(cnt)
                
                # check if contour is big enough for MOSSE.
                area = cv2.contourArea(cnt)
                if area > threshArea:

                    x1, y1, w, h = cv2.boundingRect(cnt)
                    mosseRect = (x1, y1, w, h)

                    if (
                        (x1,y1) not in boundary # top left
                        or (x1+w, y1+w) not in boundary # top right
                        or (x1+h, y1+h) not in boundary # bottom left
                        or ((x1+w)+h, (y1+w)+h) not in boundary # bottom right
                    ):
                        break
                    else:   
                        bboxes.append(mosseRect)

            groupedBoxes,weights = cv2.groupRectangles(bboxes, 0)
            objects, boxes, success = ct.update(groupedBoxes, frame)

            mergedBoxes = []

            # BGS movement tracking
            for rectangleOfInterest in groupedBoxes:
                x,y,w,h = rectangleOfInterest
                x,y,w,h = int(x),int(y),int(w),int(h)
                # Draw the rectangles based on the different points of the rectangle of interest.
                point1 = (x, y)
                point2 = (x + w, y + h)
                mergedBoxes.append((x,y,w,h))
                if checkBgs.get() == 1:
                    # Draw rectangles on the image and the binary window image.
                    cv2.rectangle(frame, point1, point2,(0,255,0), 2)
        
            # Centroids for tracked objects
            for (objectID, centroid) in objects.items():
                # draw both the ID of the object and the centroid of the
                # object on the output frame 
                text = "# {}".format(objectID)
                cv2.putText(frame, text, (centroid[0] - 20, centroid[1] - 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
                cv2.circle(frame, (centroid[0], centroid[1]), 2, (0, 255, 0), -1)
                
            # draw MOSSE and KALMAN tracked objects 
            for i, newbox in enumerate(boxes):
                x,y,w,h = newbox
                x,y,w,h = int(x),int(y),int(w),int(h)
                p1 = (x, y)
                p2 = (x + w, y + h)
                mergedBoxes.append((x,y,w,h))
                if checkMosse.get() == 1:
                    cv2.rectangle(frame, p1, p2, (0,0,255), 2, 1)

                # returns x,y,xVelicotiy,yVelicoity 
                kalmanPredict = kt.Estimate(newbox[0],newbox[1])
                kalx1 = kalmanPredict[0] 
                kaly1 = kalmanPredict[1]
                kal1 = (kalx1, kaly1)
                kalx = kalmanPredict[0] + w
                kaly = kalmanPredict[1] + h
                kal2 = (kalx, kaly)
                mergedBoxes.append((int(kalx1),int(kaly1),w,h))
                if checkKalman.get() == 1:
                    cv2.rectangle(frame, kal1, kal2, (255,0,0),2)

            if checkMerged.get() == 1:
                mergedBoxes, weights = cv2.groupRectangles(mergedBoxes, 1)
                for mergedBox in mergedBoxes:
                    x,y,w,h = mergedBox
                    p1 = (x, y)
                    p2 = (x + w, y + h)
                    cv2.rectangle(frame, p1, p2, (255,255,255), 2, 1)

            # Add array to our mergedbox arraylist, also add frame num to start of arr
            # this way we can check accuracy of each frame calculation.
            if checkAcc.get() == 1:
                mergedBoxClone = []
                for mergedBox in mergedBoxes:
                    # reverse adds to array as (frame, 0, boxInfo)
                    mergedBox = np.insert(mergedBox,0,cap.get(cv2.CAP_PROP_POS_FRAMES)) # Get current frame number
                    mergedBoxClone.append(mergedBox.tolist()) # .tolist() to remove numpy annotations when printing.                
                mergedBoxesArrays.append(mergedBoxClone)

            cv2.namedWindow("bgs", cv2.WINDOW_NORMAL)
            cv2.imshow("bgs",img_blur_otsu_thresh)

            cv2.namedWindow("video", cv2.WINDOW_NORMAL)
            cv2.imshow("video",frame)

            # VIDEO PLAYBACK

            # pause = p
            if key == ord('p'):
                while True:
                    key2 = cv2.waitKey(1) or 0xff

                    # if q is pressed or esc
                    if key2 == ord('q') or key == 27:
                        self.QuitPlayback(cap)
                        break

                    # p again, or c if they don't read text widget maybe.
                    if key2 == ord('p') or key2 == ord('c'):
                        break
            
            # if q is pressed or esc
            if key == ord('q') or key == 27:
                self.QuitPlayback(cap)

        if checkAcc.get() == 1:
            Evaluator().Analyse(mergedBoxesArrays)

        self.QuitPlayback(cap)

    def QuitPlayback(self, cap):
        cap.release()
        cv2.destroyAllWindows()

    # Quit App command
    def Quit(self):
        global root
        root.quit()

    # Set init button checked value (1 = on, 0 = off)
    def SetButtons(self, val):
        global checkBgs, checkMosse, checkKalman, checkMerged, checkAcc
        checkBgs.set(val)
        checkMosse.set(val)
        checkKalman.set(val)
        checkMerged.set(val)

    # Setup UI
    def __init__(self, parent, *args, **kwargs):
        global checkBgs, checkMosse, checkKalman, threshSpin, checkAcc

        root.resizable(width=False, height=False)
        root.geometry('{}x{}'.format(200, 250))

        Label(root, text="Press Q or ESC to quit\nPress P to pause/play\n").grid(row=0, columnspan=2)

        threshSpin = StringVar(root)
        Label(root, text="Threshold (px):").grid(row=1, column=0, sticky=N+E)
        Spinbox(root, values=list(range(50, 10000, 50)), width=10,textvariable=threshSpin).grid(row=1, column=1, sticky=N+W)
        threshSpin.set(2500)

        Checkbutton(root, text="BGS - green", variable=checkBgs, 
            onvalue=1, offvalue=0, height=1, 
            width = 20).grid(row=2, columnspan=2, padx=1, pady=1)
        Checkbutton(root, text="MOSSE - red", variable=checkMosse, 
            onvalue=1, offvalue=0, height=1, 
            width = 20).grid(row=3, columnspan=2, padx=1, pady=1)
        Checkbutton(root, text="KALMAN - blue", variable=checkKalman, 
            onvalue=1, offvalue=0, height=1, 
            width = 20).grid(row=4, columnspan=2, padx=1, pady=1)
        Checkbutton(root, text="Merged - white", variable=checkMerged, 
            onvalue=1, offvalue=0, height=1, 
            width = 20).grid(row=5, columnspan=2, padx=1, pady=1, sticky=N)
        Checkbutton(root, text="Accuracy Calc", variable=checkAcc, 
            onvalue=1, offvalue=0, height=1, 
            width = 20).grid(row=6, columnspan=2, padx=1, pady=1, sticky=N)

        self.SetButtons(1)
        funcCommand = self.MainFunctions
        quitCommand = self.Quit
        Button(root, text="Choose Video", command=funcCommand).grid(row=7,column=0,sticky=N+E+S+W)
        Button(root, text="Quit", command=quitCommand).grid(row=7,column=1,sticky=N+E+S+W)

        root.grid_columnconfigure(0, weight=1)
        root.grid_columnconfigure(1, weight=1)
        root.grid_rowconfigure(0, weight=1)
        root.grid_rowconfigure(1, weight=1)
        root.grid_rowconfigure(5, weight=1)
        root.grid_rowconfigure(6, weight=1)
        root.grid_rowconfigure(7, weight=1)
    
            
root = Tk()
root.title("Menu")

# Checkboxes default vals
checkBgs = IntVar()
checkMosse = IntVar()
checkKalman = IntVar()
checkMerged = IntVar()
checkAcc = IntVar()

Application(root)
root.mainloop()