# jwhmorris - 2019
# Motion Detection/Tracking/Prediction with OpenCV 
# Background Subtraction/Mosse/Kalman methods

import cv2
import numpy as np

# ------------------------------------------------------- #

class Kalman:

    # Create Kalman Filter intialisation matricies 
    kf = cv2.KalmanFilter(4, 2)
    kf.measurementMatrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]], np.float32)
    kf.transitionMatrix = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]], np.float32)

    def Estimate(self, coordX, coordY):
        # Measure then predict based off measurement correction.
        measured = np.array([[np.float32(coordX)], [np.float32(coordY)]])
        self.kf.correct(measured)
        predicted = self.kf.predict()
        return predicted

        