# Motion Tracker JM

## Intro ##
**Updated: 2024, note:** "My university dissertation in 2019 involved using Open CV/python with - Background Subtraction, MOSSE, and Kalman tracker combined, getting the best possible results against occlusions and shadows whilst being able to find it after losing 'the object' also."

The project attempted to showcase good results for moving object tracking with the different methods mentioned, and see the pros/cons of each and potential future improvements. Since taking another look at it, for results in 2024, AI object tracking would be the obvious choice, but the aim in 2019 was to give an analysis on different tracking methods.

## TO INSTALL ##
Due to specific version incompatabilities you will need to download a specific version of python, and requirements from the requirements.txt as follows:

-  python 3.6 will be needed. This can be downloaded here: `https://www.python.org/downloads/release/python-360/`

- download packages from requirements txt as follows for Python 6 `py -3.6 -m pip install -r requirements.txt`, and for Python 7 release, `py -3.7 -m pip install -r requirements.txt`


## TO RUN  ##
  
For Python 6 installation (if other versions of Python exist):
-  `py -3.6 .\Application.py`

## Folder Struct as follows ##

* `motion-track-jm`
    * Main tracking code, readme, and requirements.txt. 
    
* `motion-track-jm/example_videos`
    * Ground Truth file located here for example video.  
    * If using Evaluation it will save at this location. 
    * Simple example video.

With the videos, choose the threshold number the same as `video_name-threshold.mp4` as this is the ideal threshold for testing, if there isn't a number after the video, then assume default.

To use the *Evaluation* function, you will need to have a Ground Truth file 
of a video, this will need to be done manually - I have used the software
called DarkLabel to get this. It will analyse the first matching frames of
a SINGLE (only 1 object evaluation at the moment implemented) object, this
will then do a sample frame length of 400 from that point.

To use the default example ground truth file, select the evaluation checkbox alongside the tracker methods, then select the `strawberryman.mp4` video upon opening the motion track software. Let the video run, once it has complete, you will need to enter the gt filename, `strawberryman_gt`. Wait for it to complete the evaluation and check the example_videos folder.

After the video has completed the detection, enter the gt filename as instructed.