# jwhmorris - 2019
# Motion Detection/Tracking/Prediction with OpenCV 
# Background Subtraction/Mosse/Kalman methods

import os
import numpy as np
import cv2

class Evaluator():

    def SaveToFile(self, accArray):
        print(accArray)
        np.savetxt("./example_videos/gtData.csv", accArray, delimiter=",", fmt="%s")
        print("done?")

    def Analyse(self, mergedBoxesArr):
        # Check if folder exists and if it doesn't exist, then create folder 
        if not os.path.exists('./example_videos'):
            os.makedirs('./example_videos', exist_ok=True) 

        # For now this is hardcoded file type, and is only with one video
        # Ground truth has to be made manually - and can be done in future for
        # more evaluations.
        var = input("enter the filename of the ground truth file stored in ./example_videos: ")

        fileLoc = './example_videos/' + var + '.txt'
        
        if not os.path.exists(fileLoc):
            print("File does not exist (remember to not include .txt), please try again.")
            exit()

        with open(fileLoc) as gtFile:
            print("File found and opened.")
            gtBoxes = [[line.strip('\n').split(',')] for line in gtFile.readlines()]
            mergedBoxesArrCopy = []
            for sublist in mergedBoxesArr:
                if len(sublist) != 0:
                    mergedBoxesArrCopy.append(sublist[0])

            self.Compare(mergedBoxesArrCopy, gtBoxes)

    def Compare(self, mergedBoxArrCopy, gtBoxes):
        accArray = []

        # each gtbox is [frame#,n,x,y,w,h,label]
        for gtBox in gtBoxes:
            # each mergedBox is frame#,x,y,w,h
            for mergedBox in mergedBoxArrCopy:
                # check is right frame and not greater than testing frame number (consistent)
                if int(mergedBox[0]) == int(gtBox[0][0]) and int(mergedBox[0]) <= 400:
                    # Call bounding box IoU
                    boxA = [mergedBox[1],mergedBox[2],mergedBox[3],mergedBox[4]]
                    boxB = [gtBox[0][2],gtBox[0][3],gtBox[0][4],gtBox[0][5]]
                    iou = self.BoundingBoxIoU(boxA,boxB)
                    accArray.append([mergedBox[0], iou])

        print("saving output to file.")
        self.SaveToFile(accArray)

    # Calculate IoU using pyImageSearch example
    # we get union and calculate percent accuracy for it
    def BoundingBoxIoU(self, boxA, boxB):
        for i in range(len(boxA)):
            boxA[i] = int(boxA[i])
        for i in range(len(boxB)):
            boxB[i] = int(boxB[i])
        unio = self.union(boxA,boxB)
        unionArea = unio[2]*unio[3]
        inter = self.intersection(boxA,boxB)
        interArea = inter[2]*inter[3]

        iou = interArea/unionArea
        return iou
 

    def union(self,a,b):
        x = min(a[0], b[0])
        y = min(a[1], b[1])
        w = max(a[0]+a[2], b[0]+b[2]) - x
        h = max(a[1]+a[3], b[1]+b[3]) - y
        return (x, y, w, h)

    def intersection(self,a,b):
        x = max(a[0], b[0])
        y = max(a[1], b[1])
        w = min(a[0]+a[2], b[0]+b[2]) - x
        h = min(a[1]+a[3], b[1]+b[3]) - y
        if w<0 or h<0: return (0,0,0,0)
        return (x, y, w, h)